package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.Lote;
import ito.poo.clases.Prenda;

public class Myapp {

	static void run (){

		Prenda p1=new Prenda(132,"Lana",325.78f, "Mixto", "Verano");
	    System.out.println(p1);
	    p1.addLote(5,3500,LocalDate.now());
	    p1.addLote(7,1500,LocalDate.of(2021,3,21));
	    System.out.println();
	    System.out.println(p1);
	   
	}
	public  static  void main(String[] args) {
		 run();
	}
}
