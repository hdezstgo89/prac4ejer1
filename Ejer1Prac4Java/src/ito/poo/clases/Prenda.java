package ito.poo.clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class Prenda {
	
	private int modelo;
	private String tela;
	private float costoProduccion;
	private String genero;
	private String temporada;
	private ArrayList<Lote> lotes = new ArrayList<Lote>();
	/**********************************************************/
	public Prenda() {
		super();
	}
	
	public Prenda(int modelo, String tela, float costoProduccion, String genero, String temporada) {
		super();
		this.modelo = modelo;
		this.tela = tela;
		this.costoProduccion = costoProduccion;
		this.genero = genero;
		this.temporada = temporada;
	}
	/**********************************************************/
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	public String getTela() {
		return tela;
	}

	public void setTela(String tela) {
		this.tela = tela;
	}

	public float getCostoProduccion() {
		return costoProduccion;
	}

	public void setCostoProduccion(float costoProduccion) {
		this.costoProduccion = costoProduccion;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getTemporada() {
		return temporada;
	}

	public void setTemporada(String temporada) {
		this.temporada = temporada;
	}
	
	public ArrayList<Lote> getLote(){
		return this.lotes;
	}
	
	public void addLote(int numLote, int numPiezas, LocalDate fecha) {
		Lote L = new Lote(numLote,numPiezas,fecha);
		this.lotes.add(L);
	}
	/**********************************************************/
	public float costoxLote(float costoxUnidad) {
		float numLote = 0;
		return numLote;
	}

	@Override
	public String toString() {
		return "Prenda [modelo=" + modelo + ", tela=" + tela + ", costoProduccion=" + costoProduccion + ", genero="
				+ genero + ", temporada=" + temporada + ", lotes=" + lotes + "]";
	}
}
