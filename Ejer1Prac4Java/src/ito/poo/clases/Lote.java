package ito.poo.clases;

import java.time.LocalDate;

public class Lote {

	private int numLote;
	private int numPiezas;
    private LocalDate fecha;
	/***************************************/
	public Lote() {
		super();
	}
	public Lote(int numLote, int numPiezas, LocalDate fecha) {
		super();
		this.numLote = numLote;
		this.numPiezas = numPiezas;
		this.fecha = fecha;
	}
	/***************************************/
	public int getNumLote() {
		return numLote;
	}

	public void setNumLote(int numLote) {
		this.numLote = numLote;
	}

	public int getNumPiezas() {
		return numPiezas;
	}

	public void setNumPiezas(int numPiezas) {
		this.numPiezas = numPiezas;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	public float costoProduccion() {
		float numLote=0;
		return numLote;
	}

	public float montoProduccionxlote() {
		float numLote=0;
		return numLote;
	}

	public float montoRecuperacionxpieza() {
		float numLote=0;
		return numLote;
	}
	@Override
	public String toString() {
		return "Lote [numLote=" + numLote + ", numPiezas=" + numPiezas + ", fecha=" + fecha + "]";
	}
}
